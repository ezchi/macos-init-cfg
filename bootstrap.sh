#!/usr/bin/env bash

# Check for Homebrew, install if we don't have it
which -s brew
if [[ ${?} != 0 ]]; then
    ec-info "Installing homebrew..."

    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/uninstal)"
    sudo rm -rf /usr/local/Cellar /usr/local/.git
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
    brew update
    brew upgrade
fi

# Add Repository
brew tap homebrew/dupes
brew tap homebrew/versions
brew tap homebrew/binary
brew tap thoughtbot/formulae
brew tap caskroom/fonts

packages=(
    # recent versions of some OS X tools
    homebrew/dupes/grep

    # Shell
    bash

    # Editor
    # emacs      # Installed with cask

    cask

    # Multiplexe
    reattach-to-user-namespace
    tmux

    # Git
    gist
    git
    gitsh
    hub
    tig

    # Image
    imagemagick

    # Utils
    ag
    autoconf
    automake
    coreutils
    curl
    direnv
    fasd
    findutils
    htop
    libyaml
    llvm
    markdown
    nkf
    openssl
    peco
    proctools
    readline
    rmtrash
    tree
    wget

    # Languages
    go
)

echo "installing binaries..."
brew install ${packages[@]} && brew cleanup

echo "---> Please run \"ln -s /usr/local/Cellar/openssl/{version}/include/openssl /usr/local/include/openssl\" to make sure openssl can be linked properly"

# Casks
brew install caskroom/cask/brew-cask

# Apps
apps=(
    cmake
    emacs
)

# Install apps to /Applications
echo "installing apps..."
brew cask install --appdir="/Applications" ${apps[@]}

# fonts
fonts=(
    font-m-plus
    font-source-code-pro
    font-source-code-pro-for-powerline
    font-clear-sans
    font-roboto
)

# install fonts
echo "installing fonts..."
brew cask install ${fonts[@]}
